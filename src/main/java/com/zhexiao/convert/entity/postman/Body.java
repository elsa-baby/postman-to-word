package com.zhexiao.convert.entity.postman;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author zhe.xiao
 * @date 2021-01-12
 * @description
 */
@Data
@Accessors(chain = true)
public class Body {
    private String mode;

    private String raw;
}
