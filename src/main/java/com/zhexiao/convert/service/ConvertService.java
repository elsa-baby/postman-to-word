package com.zhexiao.convert.service;

import com.zhexiao.convert.base.Result;
import com.zhexiao.convert.entity.dto.ConvertDTO;
import com.zhexiao.convert.entity.postman.Postman;
import com.zhexiao.convert.entity.vo.UploaderFileVO;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author zhe.xiao
 * @date 2020/09/16
 * @description
 */
public interface ConvertService {

    /**
     * 解析上传的文件数据
     *
     * @param file
     * @return
     */
    Result<Postman> upload(MultipartFile file);

    /**
     * 生成word数据
     *
     * @param file
     * @return
     * @throws Exception
     */
    Result<String> writeWord(MultipartFile file, ConvertDTO convertDTO);
}
